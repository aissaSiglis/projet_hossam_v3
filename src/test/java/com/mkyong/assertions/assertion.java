package com.mkyong.assertions;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class assertion {

    // Test for assertEquals()
    @Test
    public void testAssertEquals() {
        assertEquals(4, add(2, 2));
    }

    // Test for assertNotEquals()
    @Test
    public void testAssertNotEquals() {
        assertNotEquals(5, add(2, 2));
    }

    // Test for assertNull()
    @Test
    public void testAssertNull() {
        assertNull(getNullString());
    }


    // Test for assertTrue()
    @Test
    public void testAssertTrue() {
        assertTrue(isEven(4));
    }

    // Test for assertFalse()
    @Test
    public void testAssertFalse() {
        assertFalse(isEven(3));
    }

    // Test for fail()
    @Test
    public void testFail() {
        fail("This test intentionally fails.");
    }

    // Sample method for addition
    public int add(int a, int b) {
        return a + b;
    }

    // Sample method returning null
    public String getNullString() {
        return null;
    }

    // Sample method checking if a number is even
    public boolean isEven(int num) {
        return num % 2 == 0;
    }

}
